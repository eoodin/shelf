package online.eoodin.shelf.model;

public class AccessorInfo {
    private ShelfUser user;

    public ShelfUser getUser() {
        return user;
    }

    public void setUser(ShelfUser user) {
        this.user = user;
    }
}
