package online.eoodin.shelf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShelfServerApplication {
	public static void main(String[] args) {
		SpringApplication.run(ShelfServerApplication.class, args);
	}
}
